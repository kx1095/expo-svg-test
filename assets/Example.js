/** this component was created with svg-to-exporeact by KX1095. **/
import React        from 'react';
import PropTypes    from 'prop-types';
import {Svg}        from 'expo';

const {
	      Defs,
	      RadialGradient,
	      Stop,
	      ClipPath,
	      Circle,
	      Mask,
	      G,
	      Text,
	      Rect,
      } = Svg;

const Example = (props) => {
	return (
		<Svg
			version={""}
			xmlns={""}
			preserveAspectRatio={"xMidYMid meet"}
			viewBox={"0 0 200 200"}
			width={"200"}
			height={"200"}
		>
			<Defs>
				<RadialGradient
					id={"grad"}
					cx={props.grad_cx !== undefined ? props.grad_cx : "50%"}
					cy={props.grad_cy !== undefined ? props.grad_cy : "50%"}
					fx={props.grad_fx !== undefined ? props.grad_fx : "50%"}
					fy={props.grad_fy !== undefined ? props.grad_fy : "50%"}
				>
					<Stop
						id={"gradStopMid"}
						offset={props.gradStopMid_offset !== undefined ? props.gradStopMid_offset : "0%"}
						stopColor={props.gradStopMid_stopColor !== undefined ? props.gradStopMid_stopColor : "#00f"}
						stopOpacity={"1"}
					/>
					<Stop
						id={"gradStopOut"}
						offset={props.gradStopOut_offset !== undefined ? props.gradStopOut_offset : "100%"}
						stopColor={props.gradStopOut_stopColor !== undefined ? props.gradStopOut_stopColor : "#004"}
						stopOpacity={"1"}
					/>
				</RadialGradient>
				<ClipPath
					id={"textBackground1"}
				>
					<Circle
						cx={"100"}
						cy={"100"}
						r={"100"}
					/>
				</ClipPath>
				<Mask
					id={"maskTest"}
					width={props.maskTest_width !== undefined ? props.maskTest_width : '100%'}
					height={props.maskTest_height !== undefined ? props.maskTest_height : '100%'}
					x={props.maskTest_x !== undefined ? props.maskTest_x : 0}
					y={props.maskTest_y !== undefined ? props.maskTest_y : 0}
				>
					<G>
						<Circle
							r={"100"}
							cy={"100"}
							cx={"100"}
							fill={"#FFFFFF"}
						/>
						<Text
						fontSize={"45"}
						fontFamily={"sans-serif"}
						textAnchor={"middle"}
						x={"100"}
						y={"115"}
						fill={"#000000"}
					>
						Example
					</Text>

					</G>

				</Mask>

			</Defs>
			<Rect
				width={"200"}
				height={"200"}
				y={"0"}
				x={"0"}
				fill={"url(#grad)"}
				mask={"url(#maskTest)"}
			/>
		</Svg>
	);
};

Example.propTypes = {
	grad_cx: PropTypes.string,
	grad_cy: PropTypes.string,
	grad_fx: PropTypes.string,
	grad_fy: PropTypes.string,
	gradStopMid_offset: PropTypes.string,
	gradStopMid_stopColor: PropTypes.string,
	gradStopOut_offset: PropTypes.string,
	gradStopOut_stopColor: PropTypes.string,
	maskTest_width: PropTypes.string,
	maskTest_height: PropTypes.string,
	maskTest_x: PropTypes.string,
	maskTest_y: PropTypes.string
};

Example.defaultProps = {
	grad_cx: "50%",
	grad_cy: "50%",
	grad_fx: "50%",
	grad_fy: "50%",
	gradStopMid_offset: "0%",
	gradStopMid_stopColor: "#00f",
	gradStopOut_offset: "100%",
	gradStopOut_stopColor: "#004",
	maskTest_width: "100%",
	maskTest_height: "100%",
	maskTest_x: "0",
	maskTest_y: "0"
};
export default Example;
	