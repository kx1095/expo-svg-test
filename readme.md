# Expo-Svg-Test
This is a small and simple Expo-App for testing of converted SVG-Files.

You can simply convert your SVG-File with `svg-to-exporeact` cli.

A Example-Svg file, for testing the app and options you have with a converted, is included.

## `Svg-To-ExpoReact` installation
For installation of my converter-cli-program make sure you have installed node.js and `npm` is added to your PATH. \
Then open your Terminal and execute following command:
```
npm i -g svg-to-exporeact
```