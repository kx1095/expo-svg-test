import React from 'react';
import { View, Text, TouchableNativeFeedback } from 'react-native';
import Example from "./assets/Example";

export default class App extends React.Component
{
	constructor(props) {
		super(props);
		this.state = {
			selectedMidColor: '#003b88',
			selectedOutColor: '#989898',
			testColors:       [
				'#ff6a00',
				'#00600d',
				'#003b88',
				'#be8800',
				'#712b07',
				'#989898'
			],
			selectedBackgroundColor: '#FFFFFF',
			availableBgColors: [
				'#FFFFFF',
				'#DDDD7F',
				'#333333',
				'#111111',
			],
		};

		this.selectStopMidColor = this.selectStopMidColor.bind(this);
		this.selectStopOutColor = this.selectStopOutColor.bind(this);
		this.selectBgColor = this.selectBgColor.bind(this);
	}

	selectStopMidColor(newColor) {
		this.setState({
			selectedMidColor: newColor,
		});
	}
	selectStopOutColor(newColor) {
		this.setState({
			selectedOutColor: newColor
		});
	}
	selectBgColor(newColor) {
		this.setState({
			selectedBackgroundColor: newColor,
		});
	}

	render() {
		return (
			<View
				style={{
					flex: 1,
					paddingTop: 45,
					paddingHorizontal: 30,
					justifyContent: 'center',
					alignItems: 'center',
					backgroundColor: this.state.selectedBackgroundColor,
				}}
			>
				<View style={{flex: 4, justifyContent: 'space-around', alignItems: 'center'}}>
					<View style={{flex: 2, paddingTop: 20}}>
						<Example
							gradStopMid_stopColor={this.state.selectedMidColor}
							gradStopOut_stopColor={this.state.selectedOutColor}
						/>
					</View>
					<View style={{flex: 2, paddingTop: 20}}>
						<View >
							<Text>
								Change Gradient Mid-Color:
							</Text>
							<View style={{ flexDirection: 'row', justifyContent: 'space-around'}}>

								{this.state.testColors.map((testCol, index) =>
									<TouchableNativeFeedback
										key={"btnView_mid-"+index}
										onPress={() => this.selectStopMidColor(testCol)}
									>
										<View
											key={"colButton_mid" + index + "-" + Date.now()}
											style={{
												width:  40,
												height: 40,
												margin: 10,
												backgroundColor: testCol
											}}
										/>
									</TouchableNativeFeedback>
								)}
							</View>
						</View>
						<View >
							<Text>
								Change Gradient Out-Color:
							</Text>
							<View style={{ flexDirection: 'row', justifyContent: 'space-around'}}>
								{this.state.testColors.map((testCol, index) =>
									<TouchableNativeFeedback
										key={"btnView_out-"+index}
										onPress={() => this.selectStopOutColor(testCol)}
									>
										<View
											key={"colButton_out" + index + "-" + Date.now()}
											style={{
												width: 40,
												height: 40,
												margin: 10,
												backgroundColor: testCol
											}}
										/>
									</TouchableNativeFeedback>
								)}
							</View>
						</View>
						<View>
							<Text>
								Change Background Color:
							</Text>
							<View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
								{this.state.availableBgColors.map((bgColor, index) =>
									<TouchableNativeFeedback
										key={"btnView_out-" + index}
										onPress={() => this.selectBgColor(bgColor)}
									>
										<View
											key={"colButton_bg" + index + "-" + Date.now()}
											style={{
												width:           40,
												height:          40,
												margin:          10,
												backgroundColor: bgColor,
												borderWidth:     1,
												borderColor:     "#555",

											}}
										/>
									</TouchableNativeFeedback>
								)}
							</View>
						</View>
					</View>
				</View>
			</View>
		);
	}
}
